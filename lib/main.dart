import 'dart:ui';

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:reg/Info.dart';
import 'package:reg/study.dart';


import 'activity.dart';
import 'calender.dart';
import 'login.dart';
import 'journey.dart';


void main() => runApp(const MyApp());

String currentPage = "LoginPage";

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        title: 'reg',
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        scrollBehavior: CustomScrollBehavior(),
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        theme: ThemeData(
          textTheme: const TextTheme(
            bodyText2: TextStyle(fontSize: 18.0),
          ),
          backgroundColor: Colors.grey.shade50,
          appBarTheme: const AppBarTheme(
            backgroundColor: Colors.grey,
          ),
          drawerTheme: DrawerThemeData(
            backgroundColor: Colors.lightBlue.shade50,
          ),
        ),
        home: LoginPage(),
      ),
    );
  }
}

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('')),
      drawer: const NavigationDrawer(),
      body: SafeArea(
        left: false,
        right: false,
        child: ListView(
          children: <Widget>[
            Container(
              color: Colors.yellow,
              child: const Text(
                'ประกาศ',
                style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 30.0),
            const Text(
                '  1.แบบประเมินความคิดเห็นของนักเรียนและนิสิตต่อการให้บริการของสำนักงานอธิการบดี(ด่วนที่สุด)'),
            const SizedBox(height: 20.0),
            const Text(
                '  ขอเชิญนิสิตร่วมทำแบบประเมินความคิดเห็นของนิสิตต่อการให้บริการของสำนักงานอธิการบดี ที่ https://bit.ly/3cyvuuf'),
            const SizedBox(height: 30.0),
            const Text('  2.การทำบัตรนิสิตกับธนาคารกรุงไทย'),
            const SizedBox(height: 20.0),
            const Text(
                '  กรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาท สำหรับนิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตรให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา ส่วนนิสิตที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง'),
            const SizedBox(height: 30.0),
            Image.network(
              'https://media.discordapp.net/attachments/793093052797419520/1070057601691308164/image.png?width=661&height=670',
              height: 300,
              width: 200,
            ),
            const SizedBox(height: 30.0),
            Image.network(
              'https://media.discordapp.net/attachments/793093052797419520/1070279426580942918/image.png',
              height: 200,
              width: 200,
            ),
            const SizedBox(height: 30.0),
            Image.network(
              'https://media.discordapp.net/attachments/793093052797419520/1070279724590436444/image.png',
              height: 200,
              width: 200,
            ),
          ],
        ),
      ),
    );
  }
}
class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const SizedBox(
            height: 115.0,
            child: DrawerHeader(
              decoration: BoxDecoration(color: Colors.deepPurple),
              child: Text(
                'เมนู',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          ListTile(
            title: const Text('หน้าหลัก'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "MainPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const MainPage(),
                ));
                currentPage = "MainPage";
              }
            },
          ),
          ListTile(
            title: const Text('ตารางเรียน/สอบ'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "StudyTablePage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const StudyTablePage(),
                ));
                currentPage = "StudyTablePage";
              }
            },
          ),
          ListTile(
            title: const Text('ประวัตินิสิต'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "StudentInfoPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const InfoPage(),
                ));
                currentPage = "StudentInfoPage";
              }
            },
          ),
          ListTile(
            title: const Text('ปฏิทิน'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "CalenderPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) =>  CalenderPage(),
                ));
                currentPage = "CalenderPage";
              }
            },
          ),
          ListTile(
            title: const Text('กิจกรรมที่น่าสนใจ'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "ActivityPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) =>  ActivityPage(),
                ));
                currentPage = "ActivityPage";
              }
            },
          ),
          ListTile(
            title: const Text('การเดินทาง'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "JourneyPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) =>  JourneyPage(),
                ));
                currentPage = "JourneyPage";
              }
            },
          ),
          ListTile(
            title: const Text('ลงทะเบียน'),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: const Text("Error"),
                      content: const Text('ยังไม่ได้อยู่ในช่วงลงทะเบียน'),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: const Text("ตกลง")),
                      ],
                    );
                  });
            },
          ),
          ListTile(
            title: const Text('ออกจากระบบ'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => LoginPage(),
              ));
              currentPage = "LoginPage";
            },
          ),
        ],
      ),
    );
  }
}
class CustomScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
  };
}
