import 'package:flutter/material.dart';

import 'main.dart';


class CalenderPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('ปฏิทิน')),
      backgroundColor: Colors.grey.shade50,
      drawer: const NavigationDrawer(),
      body: SafeArea(
        child: ListView(
          children: [
            Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 300.0),
                child: Column(
                  children: [
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292687388950548/1.png?width=947&height=670",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292687690932234/2.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292687900643439/3.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292688114565250/4.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292688408158279/5.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292688840167465/6.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292689142169600/7.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292689322516480/8.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292689540616212/9.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292750752301056/10.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292751087833088/11.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292751469527061/12.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070292751888945162/13.png?width=956&height=676",
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 20.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070298379038490634/9a65fc63d1be8d8a.png?width=799&height=670",
                      height: 300,
                      width: 300,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}