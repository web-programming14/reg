import 'package:flutter/material.dart';
import 'package:reg/main.dart';

class StudyTablePage extends StatelessWidget {
  const StudyTablePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: AppBar(title: const Text('ตารางเรียน/ตารางสอบ')),
      backgroundColor: Colors.grey.shade50,
      body: SafeArea(
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 1600.0),
                child: ListView(
                  // scrollDirection: Axis.horizontal,
                  children: [
                    Table(
                      defaultColumnWidth: const FlexColumnWidth(),
                      defaultVerticalAlignment:
                      TableCellVerticalAlignment.middle,
                      border: TableBorder.all(width: 1),
                      children: <TableRow>[
                        const TableRow(
                          decoration: BoxDecoration(
                            color: Colors.green,
                          ),
                          children: <TableCell>[
                            TableCell(
                                child: Text(
                                  '\nDay/Time\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n9:00-10:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n10:00-11:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n11:00-12:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n12:00-13:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n13:00-14:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n14:00-15:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n15:00-16:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n16:00-17:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n17:00-18:00\n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n18:00-19:00\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                        TableRow(
                          children: <TableCell>[
                            TableCell(
                                child: Container(
                                    color: Colors.grey,
                                    child: const Text(
                                      '\n\nจันทร์\n\n',
                                      textAlign: TextAlign.center,
                                    ))),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624559-59\n(3) 1, IF-4M210\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624559-59\n(3) 1, IF-4M210\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624459-59\n(3) 1, IF-3M210\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624459-59\n(3) 1, IF-3M210\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624359-59\n(3) 1, IF-3M210\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624359-59\n(3) 1, IF-3M210\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                        TableRow(
                          children: <TableCell>[
                            TableCell(
                                child: Container(
                                    color: Colors.grey,
                                    child: const Text(
                                      '\n\nอังคาร\n\n',
                                      textAlign: TextAlign.center,
                                    ))),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88634259-59\n(3) 1, IF-4C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88634259-59\n(3) 1, IF-4C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624459-59\n(3) 1, IF-4C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624459-59\n(3) 1, IF-4C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624559-59\n(3) 1, IF-3C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624559-59\n(3) 1, IF-3C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                        TableRow(
                          children: <TableCell>[
                            TableCell(
                                child: Container(
                                    color: Colors.grey,
                                    child: const Text(
                                      '\n\nพุธ\n\n',
                                      textAlign: TextAlign.center,
                                    ))),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88634459-59\n(3) 1, IF-4C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88634459-59\n(3) 1, IF-4C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88646259-59\n(3) 1, IF-6T01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88646259-59\n(3) 1, IF-6T01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88646259-59\n(3) 1, IF-6T01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624359-59\n(3) 1, IF-3C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88624359-59\n(3) 1, IF-3C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                        TableRow(
                          children: <TableCell>[
                            TableCell(
                                child: Container(
                                    color: Colors.grey,
                                    child: const Text(
                                      '\n\nพฤหัสบดี\n\n',
                                      textAlign: TextAlign.center,
                                    ))),
                            const TableCell(
                                child: Text(
                                  '\n23629164-64\n(3) 4, QS2-809\nQS2\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n23629164-64\n(3) 4, QS2-809\nQS2\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n23629164-64\n(3) 4, QS2-809\nQS2\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                        TableRow(
                          children: <TableCell>[
                            TableCell(
                                child: Container(
                                    color: Colors.grey,
                                    child: const Text(
                                      '\n\nศุกร์\n\n',
                                      textAlign: TextAlign.center,
                                    ))),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88634259-59\n(3) 1, IF-4C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88634259-59\n(3) 1, IF-4C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88634459-59\n(3) 1, IF-3C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n88634459-59\n(3) 1, IF-3C01\nIF\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                      ],
                    ),
                    Image.network(
                      'https://media.discordapp.net/attachments/793093052797419520/1073621480304291901/image.png?width=940&height=670',
                      height: 800,
                      width: 4000,
                      alignment: Alignment.center,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
