import 'package:flutter/material.dart';
import 'package:reg/main.dart';

class LoginPage extends StatelessWidget {
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();

  LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: ListView(
          children: [
            Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 300.0),
                child: Column(
                  children: [
                    const SizedBox(height: 50.0),
                    Image.network(
                      'https://th.bing.com/th/id/R.1b123ccbf185644f5c48bd46dacf8cc9?rik=PwCrLglBXu%2b2ig&riu=http%3a%2f%2f2.bp.blogspot.com%2f_mFp-4uzx0rM%2fTIZAWcsUgzI%2fAAAAAAAAABk%2fmbRjI2TU3ho%2fs1600%2fLogo_buu.jpg&ehk=Kkvup%2bX2w9KVsXFcAVIDDt9tMODPrMlMfiZ0j9VJHj0%3d&risl=&pid=ImgRaw&r=0',
                      height: 300,
                      width: 300,
                    ),
                    const SizedBox(height: 50.0),
                    TextField(
                        controller: _username,
                        decoration:
                        const InputDecoration(labelText: 'Username')),
                    TextField(
                        controller: _password,
                        obscureText: true,
                        decoration:
                        const InputDecoration(labelText: 'Password')),
                    const SizedBox(height: 30.0),
                    ElevatedButton(
                        onPressed: () {
                          if (_username.text.isEmpty ||
                              _password.text.isEmpty) {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: const Text("Error"),
                                    content: _username.text.isEmpty &&
                                        _password.text.isEmpty
                                        ? const Text(
                                        "กรุณากรอก username และ password")
                                        : _username.text.isEmpty
                                        ? const Text("กรุณากรอก username")
                                        : const Text("กรุณากรอก password"),
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text("ตกลง")),
                                    ],
                                  );
                                });
                          }else {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                              builder: (context) => const MainPage(),
                            ));
                            currentPage = "MainPage";
                          }
                        },
                        child: const Text('Login')),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}