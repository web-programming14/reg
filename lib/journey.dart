import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

import 'main.dart';


class JourneyPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('การเดินทาง')),
      backgroundColor: Colors.grey.shade50,
      drawer: const NavigationDrawer(),
      body: ImageSlideshow(
        width: 600,
        height: 800,
        initialPage: 0,
        children: [
          Image.network(
            'https://media.discordapp.net/attachments/793093052797419520/1073653226630545509/image.png',
            fit: BoxFit.cover,
          ),
          Image.network(
            'https://media.discordapp.net/attachments/793093052797419520/1073653855717441636/image.png',
            fit: BoxFit.cover,
          ),
          Image.network(
            'https://media.discordapp.net/attachments/793093052797419520/1073660010044850287/image.png',
            fit: BoxFit.cover,
          ),
        ],
      ),
    );
  }
}