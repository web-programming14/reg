import 'package:flutter/material.dart';
import 'package:reg/main.dart';

class InfoPage extends StatelessWidget {
  const InfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: AppBar(title: const Text('ระเบียนประวัติ')),
      body: SafeArea(
        child: ListView(
          // scrollDirection: Axis.horizontal,
          children: [
            Center(
              //padding: const EdgeInsets.all(10.0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 700.0),
                child: Column(
                  children: [
                    Image.network(
                      'https://media.discordapp.net/attachments/793093052797419520/1053695087512653944/87829.jpg?width=1005&height=670',
                      height: 200,
                      width: 200,
                      alignment: Alignment.center,
                    ),
                    const Text(
                      'ข้อมูลการศึกษา',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 32),
                    ),
                    const SizedBox(width: 10.0),
                    Table(
                      defaultColumnWidth: const FlexColumnWidth(),
                      border: TableBorder.all(width: 1),
                      children: const <TableRow>[
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('รหัสประจำตัว:')),
                            TableCell(child: Text('63160075')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('เลขที่บัตรประชาชน:')),
                            TableCell(child: Text('1234567897854')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('ชื่อ:')),
                            TableCell(child: Text('นางสาวธัญวรัตน์ จันเสาร์')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('ชื่ออังกฤษ:')),
                            TableCell(child: Text('Miss. THUNWARAT GHUNSAO')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('คณะ:')),
                            TableCell(child: Text('คณะวิทยาการสารสนเทศ')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('วิทยาเขต:')),
                            TableCell(child: Text('บางแสน')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('หลักสูตร:')),
                            TableCell(child: Text('2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('วิชาโท:')),
                            TableCell(child: Text('-')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('ระดับการศึกษา:')),
                            TableCell(child: Text('ปริญญาตรี')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('ชื่อปริญญา:')),
                            TableCell(child: Text('วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('ปีการศึกษาที่เข้า:')),
                            TableCell(child: Text('2563 / 1 วันที่ 29/4/2563')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('สถานภาพ:')),
                            TableCell(child: Text('-')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('วิธีรับเข้า:')),
                            TableCell(child: Text('โครงการภาคตะวันออก12จังหวัด')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('วุฒิก่อนเข้ารับการศึกษา:')),
                            TableCell(child: Text('ม.6 3.5')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('จบการศึกษาจาก:')),
                            TableCell(child: Text('อ่างศิลาพิทยาคม')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightGreenAccent,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('อ. ที่ปรึกษา:')),
                            TableCell(child: Text('ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม')),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(width: 10.0),
                    const Text(
                      'ผลการศึกษา',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 32),
                    ),
                    Table(
                      defaultColumnWidth: const FlexColumnWidth(),
                      border: TableBorder.all(width: 1),
                      children: const <TableRow>[
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.yellow,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('หน่วยกิตคำนวณ 90')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.yellow,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('หน่วยกิตที่ผ่าน 90')),
                          ],
                        ),
                        TableRow(
                          decoration: BoxDecoration(
                            color: Colors.yellow,
                          ),
                          children: <TableCell>[
                            TableCell(child: Text('คะแนนเฉลี่ยสะสม 3.32')),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
