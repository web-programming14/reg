import 'package:flutter/material.dart';

import 'main.dart';


class ActivityPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('กิจกรรมที่น่าสนใจ')),
      backgroundColor: Colors.grey.shade50,
      drawer: const NavigationDrawer(),
      body: SafeArea(
        child: ListView(
          children: [
            Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 300.0),
                child: Column(
                  children: [
                    const SizedBox(height: 30.0),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1073629389671108679/image.png",
                    ),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1070307577751146527/image.png",
                      height: 300,
                      width: 300,
                    ),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1073631273995743364/image.png",
                    ),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1073626604816842822/image.png",
                      height: 300,
                      width: 400,
                    ),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1073633969905598464/image.png",
                    ),
                    Image.network(
                      "https://media.discordapp.net/attachments/793093052797419520/1073627718530367578/image.png",
                      height: 300,
                      width: 400,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}